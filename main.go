package main

import (
	"RESTFULAPI/config"
	"RESTFULAPI/controller"
	"RESTFULAPI/data/response"
	"RESTFULAPI/helper"
	"RESTFULAPI/model"
	"RESTFULAPI/repository"
	"RESTFULAPI/router"
	"RESTFULAPI/service"
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/go-redis/redis"
	"github.com/rs/zerolog/log"
)

var redisClient *redis.Client

func init() {
	redisClient = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379", // Địa chỉ Redis server
		Password: "",               // Mật khẩu Redis server (nếu có)
		DB:       0,                // Chọn database trong Redis server
	})

	// Kiểm tra kết nối Redis server
	_, err := redisClient.Ping().Result()
	if err != nil {
		panic(err)
	}
}

var SECRET = []byte("super-secret-auth-key")
var api_key = "1234"

func CreateJWT() (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["roleName"] = "Admin"
	claims["userName"] = "hohoangan"
	claims["exp"] = time.Now().Add(time.Minute).Unix() //1 gio
	tokenStr, err := token.SignedString(SECRET)
	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}
	return tokenStr, err
}

func ValidateJWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		if tokenString := c.GetHeader("Token"); tokenString != "" {
			token, err := jwt.Parse(tokenString, func(t *jwt.Token) (interface{}, error) {
				_, ok := t.Method.(*jwt.SigningMethodHMAC)
				if !ok {
					c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "not authorized"})
					return nil, fmt.Errorf("not authorized")
				}
				return SECRET, nil
			})
			if err != nil {
				c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "not authorized: " + err.Error()})
				return
			}
			if token.Valid {
				// Lấy thông tin từ JWT (roleName)
				claims := token.Claims.(jwt.MapClaims)
				roleName := claims["roleName"].(string)

				// Lưu thông tin quyền truy cập vào context để sử dụng trong các xử lý tiếp theo
				c.Set("roleName", roleName)

				c.Next()
				return
			}
		}

		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "not authorized"})
	}
}

func GetJWT(c *gin.Context) {
	if access := c.GetHeader("Access"); access != "" {
		if access == api_key {
			token, err := CreateJWT()
			if err != nil {
				fmt.Println("Error creating JWT")
				c.AbortWithStatus(http.StatusInternalServerError)
				return
			}
			webRespone := response.Response{
				Code:   http.StatusOK,
				Status: "Ok",
				Data:   token,
			}
			c.JSON(http.StatusOK, webRespone)
			return
		}
	}
	err := response.Response{
		Code:   http.StatusForbidden,
		Status: "Access denied",
		Data:   nil,
	}
	c.AbortWithStatusJSON(http.StatusUnauthorized, err)
}

func Home(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "super secret area")
}
func main() {
	log.Info().Msg("Started Server")
	// Database
	db := config.DatabaseConnection()
	validate := validator.New()
	db.Table("NhanVien").AutoMigrate(&model.NhanVien{})
	// Repository
	nhanvienRepository := repository.NewNhanVienRepositoryImpl(db)
	// Service
	nhanvienService := service.NewNhanVienServiceimpl(nhanvienRepository, validate)
	// Controller
	nhanvienController := controller.NewNhanVienController(nhanvienService, validate, redisClient)
	// Routes
	validateJWT := ValidateJWT()
	routes := router.NewRouter(nhanvienController, validateJWT)
	routes.GET("/jwt", GetJWT)
	err := routes.Run(":8081")
	helper.EroorPanic(err)
}
