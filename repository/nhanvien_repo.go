package repository

import (
	"RESTFULAPI/model"
)

type NhanVienRepository interface {
	Save(nhanvien model.NhanVien)
	Update(nhanvien model.NhanVien)
	Delete(maNV int)
	FindById(maNV int) (nhavien model.NhanVien, err error)
	FindAll() []model.NhanVien
}
