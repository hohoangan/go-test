package repository

import (
	"RESTFULAPI/data/request"
	"RESTFULAPI/helper"
	"RESTFULAPI/model"
	"errors"

	"gorm.io/gorm"
)

type NhanVienRepositoryImpl struct {
	Db *gorm.DB
}

// Delete implements NhanVienRepository
func (t *NhanVienRepositoryImpl) Delete(maNV int) {
	var nhanvien model.NhanVien
	result := t.Db.Where("MaNV = ?", maNV).Delete(&nhanvien)
	helper.EroorPanic(result.Error)
}

// FindAll implements NhanVienRepository
func (t *NhanVienRepositoryImpl) FindAll() []model.NhanVien {
	var nhanviens []model.NhanVien
	result := t.Db.Find(&nhanviens)
	helper.EroorPanic(result.Error)
	return nhanviens
}

// FindById implements NhanVienRepository
func (t *NhanVienRepositoryImpl) FindById(maNV int) (nhavien model.NhanVien, err error) {
	var nhanvien model.NhanVien
	result := t.Db.Find(&nhanvien, maNV)
	if result != nil {
		return nhanvien, nil
	} else {
		return nhanvien, errors.New("NhanVien is not found")
	}
}

// Save implements NhanVienRepository
func (t *NhanVienRepositoryImpl) Save(nhanvien model.NhanVien) {
	result := t.Db.Create(&nhanvien)
	helper.EroorPanic(result.Error)
}

// Update implements NhanVienRepository
func (t *NhanVienRepositoryImpl) Update(nhanvien model.NhanVien) {
	var updateNhanVien = request.UpdateNhanVienRequest{
		MaNV:  nhanvien.MaNV,
		TenNV: nhanvien.TenNV,
	}
	result := t.Db.Model(&nhanvien).Updates(map[string]interface{}{
		"MaNV":  updateNhanVien.MaNV,
		"TenNV": updateNhanVien.TenNV,
	})
	helper.EroorPanic(result.Error)
}

func NewNhanVienRepositoryImpl(Db *gorm.DB) NhanVienRepository {
	return &NhanVienRepositoryImpl{Db: Db}
}
