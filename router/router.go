package router

import (
	"RESTFULAPI/controller"
	"RESTFULAPI/data/response"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func contains(slice []string, item string) bool {
	for _, s := range slice {
		if strings.EqualFold(s, item) {
			return true
		}
	}
	return false
}

func CheckRoleAccess(roleNames []string) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Kiểm tra quyền truy cập dựa trên roleName
		userRoleName, exists := c.Get("roleName")
		if !exists || !contains(roleNames, userRoleName.(string)) {
			webRespone := response.Response{
				Code:   http.StatusForbidden,
				Status: "Access denied",
				Data:   nil,
			}
			c.AbortWithStatusJSON(http.StatusForbidden, webRespone)
			return
		}

		// Cho phép đi tiếp đến handler API
		c.Next()
	}
}

func NewRouter(nhanvienController *controller.NhanVienController, validateJWT gin.HandlerFunc) *gin.Engine {
	routes := gin.Default()
	routes.GET("", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, "wellcome home")
	})
	baseRouter := routes.Group("/api")
	baseRouter.Use(validateJWT, CheckRoleAccess([]string{"Admin", "SuperAdmin"})) // Áp dụng middleware cho nhóm route
	{
		nhanvienRouter := baseRouter.Group("/nhanvien")
		nhanvienRouter.GET("", nhanvienController.FindAll)
		nhanvienRouter.GET("/:MaNV", nhanvienController.FindById)
		nhanvienRouter.POST("", nhanvienController.Create)
		nhanvienRouter.DELETE("/:MaNV", nhanvienController.Delete)
		nhanvienRouter.PATCH("/:MaNV", nhanvienController.Update)
	}
	baseRouter.Use(validateJWT, CheckRoleAccess([]string{"User"})) // Áp dụng middleware cho nhóm route
	{
		svRouter := baseRouter.Group("/sinhvien")
		svRouter.GET("", nhanvienController.FindAll)
	}
	return routes
}
