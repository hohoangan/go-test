package model

type NhanVien struct {
	MaNV  int    `gorm:"column:MaNV;type:int;primary_key"`
	TenNV string `gorm:"column:TenNV;type:varchar(255)"`
}

// Trong trường hợp không muốn sử dụng tên mặc định của bảng NhanVien, bạn có thể chỉ định tên bảng là "NhanVien"
func (NhanVien) TableName() string {
	return "NhanVien"
}
