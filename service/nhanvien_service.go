package service

import (
	"RESTFULAPI/data/request"
	"RESTFULAPI/data/response"
)

type NhanVienService interface {
	Create(nhanvien request.CreateNhanVienRequest)
	Update(nhanvien request.UpdateNhanVienRequest)
	Delete(maNV int)
	FindById(maNV int) response.NhanVienResponse
	FindAll() []response.NhanVienResponse
}
