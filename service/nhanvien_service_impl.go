package service

import (
	"RESTFULAPI/data/request"
	"RESTFULAPI/data/response"
	"RESTFULAPI/helper"
	"RESTFULAPI/model"
	"RESTFULAPI/repository"

	"github.com/go-playground/validator/v10"
)

type NhanVienServiceimpl struct {
	NhanvienRepository repository.NhanVienRepository
	Validate           *validator.Validate
}

// Create implements NhanVienService
func (t *NhanVienServiceimpl) Create(nhanvien request.CreateNhanVienRequest) {
	err := t.Validate.Struct(nhanvien)
	helper.EroorPanic(err)
	nvModel := model.NhanVien{
		TenNV: nhanvien.TenNV,
	}
	t.NhanvienRepository.Save(nvModel)
}

// Delete implements NhanVienService
func (t *NhanVienServiceimpl) Delete(maNV int) {
	t.NhanvienRepository.Delete(maNV)
}

// FindAll implements NhanVienService
func (t *NhanVienServiceimpl) FindAll() []response.NhanVienResponse {
	result := t.NhanvienRepository.FindAll()
	var nvs []response.NhanVienResponse
	for _, value := range result {
		nv := response.NhanVienResponse{
			MaNV:  value.MaNV,
			TenNV: value.TenNV,
		}
		nvs = append(nvs, nv)
	}
	return nvs
}

// FindById implements NhanVienService
func (t *NhanVienServiceimpl) FindById(maNV int) response.NhanVienResponse {
	result, err := t.NhanvienRepository.FindById(maNV)
	helper.EroorPanic(err)
	nvRepo := response.NhanVienResponse{
		MaNV:  result.MaNV,
		TenNV: result.TenNV,
	}
	return nvRepo
}

// Update implements NhanVienService
func (t *NhanVienServiceimpl) Update(nhanvien request.UpdateNhanVienRequest) {
	nvData, err := t.NhanvienRepository.FindById(nhanvien.MaNV)
	helper.EroorPanic(err)
	nvData.TenNV = nhanvien.TenNV
	t.NhanvienRepository.Update(nvData)
}

func NewNhanVienServiceimpl(nhanvienRepository repository.NhanVienRepository, validate *validator.Validate) NhanVienService {
	return &NhanVienServiceimpl{
		NhanvienRepository: nhanvienRepository,
		Validate:           validate,
	}
}
