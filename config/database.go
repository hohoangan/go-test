package config

import (
	"RESTFULAPI/helper"
	"fmt"

	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
)

const (
	host     = "localhost"
	port     = 1433
	user     = ""
	password = ""
	dbname   = "QuanLyCuaHangNoiThat"
)

func DatabaseConnection() *gorm.DB {
	sqlInfo := fmt.Sprintf("sqlserver://%s:%d?database=%s", host, port, dbname)
	db, err := gorm.Open(sqlserver.Open(sqlInfo), &gorm.Config{})
	helper.EroorPanic(err)
	return db
}
