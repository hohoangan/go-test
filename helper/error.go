package helper

import (
	"RESTFULAPI/data/response"

	"github.com/go-playground/validator/v10"
)

func EroorPanic(err error) {
	if err != nil {
		panic(err)
	}
}

type ValidationError struct {
	Field   string `json:"field"`
	Message string `json:"message"`
}

func HandleValidationError(err error) []ValidationError {
	var validationErrs []ValidationError

	if validationErrors, ok := err.(validator.ValidationErrors); ok {
		for _, fieldErr := range validationErrors {
			field := fieldErr.Field()
			message := fieldErr.Tag()

			validationError := ValidationError{
				Field:   field,
				Message: message,
			}

			validationErrs = append(validationErrs, validationError)
		}
	}

	return validationErrs
}

func CreateResponse(code int, status string, data interface{}) response.Response {
	return response.Response{
		Code:   code,
		Status: status,
		Data:   data,
	}
}
