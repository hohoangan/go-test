package controller

import (
	"RESTFULAPI/data/request"
	"RESTFULAPI/data/response"
	"RESTFULAPI/helper"
	"RESTFULAPI/service"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/go-redis/redis"
)

type NhanVienController struct {
	nhanvienService service.NhanVienService
	Validate        *validator.Validate
	RedisClient     *redis.Client // Thêm trường RedisClient
}

func NewNhanVienController(service service.NhanVienService, validate *validator.Validate, redisClient *redis.Client) *NhanVienController {
	return &NhanVienController{
		nhanvienService: service,
		Validate:        validate,
		RedisClient:     redisClient, // Thêm tham số redisClient
	}
}

// Create
func (controller *NhanVienController) Create(ctx *gin.Context) {
	createRequest := request.CreateNhanVienRequest{}
	err := ctx.ShouldBindJSON(&createRequest)
	helper.EroorPanic(err)
	controller.nhanvienService.Create(createRequest)
	webRespone := response.Response{
		Code:   http.StatusOK,
		Status: "OK",
		Data:   nil,
	}
	ctx.JSON(http.StatusOK, webRespone)
}

// Update
func (controller *NhanVienController) Update(ctx *gin.Context) {
	updateRequest := request.UpdateNhanVienRequest{}
	err := ctx.ShouldBindJSON(&updateRequest)
	helper.EroorPanic(err)

	MaNhanVien := ctx.Param("MaNV")
	maNV, err := strconv.Atoi(MaNhanVien)
	helper.EroorPanic(err)

	// Perform validation
	err = controller.Validate.Struct(updateRequest)
	if err != nil {
		validationErrors := helper.HandleValidationError(err)
		// Create error response
		webRespone := helper.CreateResponse(http.StatusBadRequest, "Lỗi validate dữ liệu", validationErrors)
		ctx.JSON(http.StatusBadRequest, webRespone)
		return
	}

	updateRequest.MaNV = maNV

	controller.nhanvienService.Update(updateRequest)
	webRespone := helper.CreateResponse(http.StatusOK, "OK", nil)
	ctx.JSON(http.StatusOK, webRespone)
}

func HandleValidationError(err error) {
	panic("unimplemented")
}

// Get all
// func (controller *NhanVienController) FindAll(ctx *gin.Context) {

// 	nvResponse := controller.nhanvienService.FindAll()
// 	webRespone := response.Response{
// 		Code:   http.StatusOK,
// 		Status: "OK",
// 		Data:   nvResponse,
// 	}
// 	ctx.Header("Content-Type", "application/json")
// 	ctx.JSON(http.StatusOK, webRespone)
// }

// Get all
func (controller *NhanVienController) FindAll(ctx *gin.Context) {
	// Kiểm tra xem danh sách nhân viên đã có trong cache hay chưa
	cachedData, err := controller.RedisClient.Get("danh_sach_nhan_vien").Result()
	if err == nil {
		// Nếu dữ liệu đã có trong cache, trả về dữ liệu từ cache
		ctx.Header("Content-Type", "application/json")
		ctx.String(http.StatusOK, cachedData)
		return
	}

	// Nếu dữ liệu chưa có trong cache, gọi API để lấy dữ liệu
	nvResponse := controller.nhanvienService.FindAll()

	// Chuyển đổi dữ liệu thành JSON
	jsonData, err := json.Marshal(nvResponse)
	if err != nil {
		// Xử lý lỗi khi chuyển đổi dữ liệu thành JSON
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to convert data to JSON"})
		return
	}

	// Lưu danh sách nhân viên vào cache 1phut
	err = controller.RedisClient.Set("danh_sach_nhan_vien", string(jsonData), 1*time.Minute).Err()
	if err != nil {
		// Xử lý lỗi khi lưu cache
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to store cache"})
		return
	}

	// Trả về dữ liệu
	ctx.Header("Content-Type", "application/json")
	ctx.String(http.StatusOK, string(jsonData))
}

// Delete
func (controller *NhanVienController) Delete(ctx *gin.Context) {
	MaNhanVien := ctx.Param("MaNV")
	maNV, err := strconv.Atoi(MaNhanVien)
	helper.EroorPanic(err)

	controller.nhanvienService.Delete(maNV)
	webRespone := response.Response{
		Code:   http.StatusOK,
		Status: "OK",
		Data:   nil,
	}
	ctx.JSON(http.StatusOK, webRespone)
}

// FindById
func (controller *NhanVienController) FindById(ctx *gin.Context) {
	MaNhanVien := ctx.Param("MaNV")
	maNV, err := strconv.Atoi(MaNhanVien)
	helper.EroorPanic(err)
	nvResponse := controller.nhanvienService.FindById(maNV)
	webRespone := response.Response{
		Code:   http.StatusOK,
		Status: "OK",
		Data:   nvResponse,
	}
	ctx.Header("Content-Type", "application/json")
	ctx.JSON(http.StatusOK, webRespone)
}
