package response

type NhanVienResponse struct {
	MaNV  int    `json:"manv"`
	TenNV string `json:"tennv"`
}
