package request

type CreateNhanVienRequest struct {
	TenNV string `validate:"required,min=1,max=200" json:"TenNV"`
}
