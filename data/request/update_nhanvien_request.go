package request

type UpdateNhanVienRequest struct {
	MaNV  int    `validate:"required"`
	TenNV string `validate:"required,min=1,max=200" json:"TenNV"`
}
